sudo gitlab-runner register --url https://gitlab.com/ \
--registration-token <registration-token> \
--executor docker \
--docker-image docker_dora_mininet \
--description "Dora Runner" \
--tag-list "dora" \
--docker-pull-policy if-not-present \
--docker-privileged true

sudo gitlab-runner register --url https://gitlab.com/ \
--registration-token <registration-token> \
--executor docker \
--docker-image docker_dora_mininet \
--description "Mininet Runner" \
--tag-list "mininet" \
--docker-pull-policy if-not-present \
--docker-privileged true