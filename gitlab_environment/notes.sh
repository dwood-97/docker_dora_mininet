You need to be running Ubuntu 20.04

install docker with following commands:
    sudo apt-get update && apt-get upgrade -y; \
    sudo apt install git -y; \
    sudo apt install apt-transport-https ca-certificates curl software-properties-common -y; \
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -; \
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu `lsb_release -cs` test" -y; \
    sudo curl -sSL https://get.docker.com/ | sudo sh; \
    sudo apt-get update && sudo apt-get upgrade; \
    sudo docker images


### START HERE TOMORROW ###
Fork the repo at  "https://gitlab.com/dwood-97/docker_dora_mininet"
git config --global credential.helper store
git config --global user.email "you@example.com"
git config --global user.name "user.nameexample"
git clone https://<username>:<personal_token>@gitlab.com/gitlab-org/gitlab.git
For the "gitlab.com/gitlab-org/gitlab.git" section, just use the https link you recieve when cloning your repo from the GUI

add user to the docker group
    sudo usermod -aG docker $USER

reboot

cd docker_dora_mininet

docker build -t docker_dora_mininet .

Install Gitlab, replace ${arch} with any of the supported architectures, e.g. amd64, arm, arm64
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_${arch}.deb"
dpkg -i gitlab-runner_<arch>.deb

Register your runners with the following commands:

    sudo gitlab-runner register --url https://gitlab.com/ \
        --registration-token <registration-token> \
        --executor docker \
        --docker-image docker_dora_mininet \
        --description "Dora Runner" \
        --tag-list "dora" \
        --docker-pull-policy if-not-present \
        --docker-privileged true

    sudo gitlab-runner register --url https://gitlab.com/ \
        --registration-token <registration-token> \
        --executor docker \
        --docker-image docker_dora_mininet \
        --description "Mininet Runner" \
        --tag-list "mininet" \
        --docker-pull-policy if-not-present \
        --docker-privileged true

Now that runners are set up and repo is set up, you can head into gitlab and test out the pipline.